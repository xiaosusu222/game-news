package com.king.dao;

import com.king.entity.PersonalTag;
import com.king.util.DBPoolUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ArrayHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.nio.file.FileSystemLoopException;
import java.sql.SQLException;
import java.util.List;

/**
 * ClassName:PersonlTag
 * Package:com.king.dao
 * Description:
 *
 * @Data:2021/9/27 23:37
 */
public class PersonalTagDao {
    BaseDao baseDao = new BaseDao();
    /*通过id获取标签信息*/
    public List<PersonalTag> getTagsByUserId(String id) {
        String sql = "select * from personal_tag where user_id = ?";
        return baseDao.queryObjects(sql,PersonalTag.class,id);
    }

}
