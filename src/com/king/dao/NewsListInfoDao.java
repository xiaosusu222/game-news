package com.king.dao;

import com.king.entity.NewsListInfo;
import com.king.util.DataBaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 查询新闻列表所需的信息
 */
public class NewsListInfoDao {
    public static List<NewsListInfo> getNewsListInfo(){
        List<NewsListInfo> newsListInfoList = new ArrayList<>();
        Connection conn = DataBaseUtil.getConnection();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String sql = "select id,new_title,title_content,cover_image,news_add_time from news limit ?";
        try {
            ps = conn.prepareStatement(sql);
            //手动输入n条数据。
            ps.setInt(1,10);
            rs = ps.executeQuery();
            while (rs.next()){
                NewsListInfo newsListInfo = new NewsListInfo();
                newsListInfo.setId(rs.getString("id"));
                newsListInfo.setNewsTitle(rs.getString("new_title"));
                newsListInfo.setTitleContent(rs.getString("title_content"));
                newsListInfo.setCoverImage(rs.getString("cover_image"));
                newsListInfo.setNewsUploadTime(rs.getString("news_add_time"));
                newsListInfoList.add(newsListInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataBaseUtil.close(conn,ps,rs);
        }
        return newsListInfoList;
    }
}
