package com.king.dao;

import com.king.entity.NewsDetail;
import com.king.util.DataBaseUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * ClassName:NewsDao
 * Package:com.king.dao
 * Description:
 *
 * @Data:2021/10/9 9:30
 */
public class NewsDao {
    private BaseDao baseDao = new BaseDao();

    public NewsDetail getNewsDetail(String newsId) {
        String sql = "select new_title,content,news_add_time,nick_name from news  left join user on news.user_id = user.id where news.id = ?";
        return baseDao.queryObject(sql,NewsDetail.class,newsId);
    }
}
