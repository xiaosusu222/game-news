package com.king.dao;

import com.king.entity.User;

import java.util.List;

public class UserDao {
    BaseDao baseDao = new BaseDao();
    /*通过userEmail获取用户信息*/
    public User getUserByUserEmail(String userEmail) {
        String sql = "select * from user where user_email = ?";
        User user = baseDao.queryObject(sql, User.class, userEmail);
        return user;
    }
    /*添加用户信息*/
    public boolean addUser(User user) {
        String sql = "insert into user(id,password,user_email,user_avatar,nick_name,user_signature) values(?,?,?,?,?,?)";
        int b = baseDao.update(sql,user.getId(),user.getPassword(),user.getUser_email(),user.getUser_avatar(),user.getNick_name(),user.getUser_signature());
        return b>0;
    }
    //修改用户信息
    public boolean updUser(User user) {
        String sql = "update user set password=?,user_email=?,user_sex=?,nick_name=?,user_signature=? where id=?";
        int a = baseDao.update(sql,user.getPassword(),user.getUser_email(),user.getUser_sex(),user.getNick_name(),user.getUser_signature(),user.getId());
        return a>0;
    }
    //通过用户id获取用户信息
    public User getUserByPwd(String password) {
        String sql = "select * from user where password = ?";
        User user = baseDao.queryObject(sql, User.class, password);
        return user;
    }
}
