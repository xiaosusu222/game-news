package com.king.web;

import com.king.entity.User;
import com.king.service.InfoService;
import com.king.util.StringUtils;

import javax.lang.model.element.VariableElement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

@WebServlet("/info")
public class infoServlet extends BaseServlet{
    //创建service对象
    InfoService service = new InfoService();

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取method
        String method = request.getParameter("method");
        HttpSession session = request.getSession();
        //为空转发到404
        if(StringUtils.isBlank(method)) request.getRequestDispatcher("404.jsp");
        //user为空跳转到登录界面
        else if(session.getAttribute("user")==null)request.getRequestDispatcher("html/login.jsp").forward(request,response);
        else{
            switch (method){
                case "settingsPage":settingsPage(request,response);break;
                case "setting":setting(request,response);break;
                case "pubPage":pubPage(request,response);break;
                case "exit":exit(request,response);break;
                case "setPwd":setPwd(request,response);break;
                case "showPic":showPic(request,response);break;

                default: break;
            }
        }
    }

    private void showPic(HttpServletRequest request, HttpServletResponse response) {
        //获取本地文件的流，然后将这个流写入到页面上去
        String pic = request.getParameter("pic");
        File file = new File(pic);
        if (file.exists()) {
            try {
                //拿到文件的输入流
                InputStream in = new FileInputStream(file);
                //输入到页面上去得用response响应
                OutputStream out = response.getOutputStream();
                //将字节写入到页面上
                byte[] b = new byte[1024];
                int len = 0;
                while ((len = in.read(b)) != -1) {
                    out.write(b, 0, len);
                }
                out.flush();
                in.close();
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //修改密码
    private void setPwd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nowpwd = request.getParameter("nowpwd");
        String pwd = request.getParameter("pwd");
        String repwd = request.getParameter("repwd");
        //获取session中的user
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        //判断密码是否正确
        if(service.checkPwd(nowpwd)){
            //验证新密码
            if(pwd.equals(repwd)){
                user.setPassword(pwd);
                //将session中的user更新
                session.setAttribute("user",user);
                //将数据库更新
                if(service.updUserInfo(user))session.setAttribute("msg","密码修改成功");
                //转发到修改个人信息界面
                request.getRequestDispatcher("html/infoSet.jsp").forward(request,response);
            }else {
                session.setAttribute("msg","旧密码与新密码不一致！！！");
                //转发到修改个人信息界面
                request.getRequestDispatcher("html/infoSet.jsp").forward(request,response);
            }
        }else {
            session.setAttribute("msg","密码输入错误");
            //转发到修改个人信息界面
            request.getRequestDispatcher("html/infoSet.jsp").forward(request,response);
        }
    }
    //退出登录
    private void exit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //删除session中的user
        request.getSession().removeAttribute("user");
        //转发到首页
        request.getRequestDispatcher("index?method=newsList").forward(request,response);
    }
    //转发到发布新闻页面
    private void pubPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        dispatcher("articlePub",request,response);
    }

    //修改个人信息
    private void setting(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取session中的user
        HttpSession session = request.getSession();
        //修改部分显示字段
        User user = (User) session.getAttribute("user");
        user.setNick_name(request.getParameter("nick_name"));
        user.setUser_sex(request.getParameter("user_sex"));
        user.setUser_email(request.getParameter("user_email"));
        user.setUser_signature(request.getParameter("user_signature"));
        //将session中的user更新
        session.setAttribute("user",user);
        //将数据库更新
        if(service.updUserInfo(user))session.setAttribute("msg","修改成功");
        //转发到修改个人信息界面
        request.getRequestDispatcher("html/infoSet.jsp").forward(request,response);
    }
    //跳转到设置界面
    private void settingsPage(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException{
        request.getRequestDispatcher("html/infoSet.jsp").forward(request,response);
    }
}

