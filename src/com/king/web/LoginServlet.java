package com.king.web;

import com.king.entity.PersonalTag;
import com.king.entity.User;
import com.king.service.LoginService;
import com.king.service.TagService;
import com.king.util.SendMail;
import com.king.util.StringUtils;

import javax.lang.model.element.VariableElement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.interfaces.RSAKey;
import java.util.List;

/**
 * ClassName:LoginServlet
 * Package:com.king.web
 * Description:
 *
 * @Data:2021/9/26 22:59
 */
@WebServlet("/login")
public class LoginServlet extends BaseServlet {

    // 创建service对象
    private LoginService ls = new LoginService();
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取前端操作符
        String method = request.getParameter("method");
        // 为空时的处理
        method = StringUtils.isBlank(method) ? "login" : method;
        switch (method){
            case "login": login(request,response);break;
            case "reg": reg(request,response);break;
            case "sendEmail": sendCode(request,response);break;
            case "codeCheck": codeCheck(request,response);break;
            case "quitLogin": quitLogin(request,response);break;
        }
    }

    /**
     * 退出登录
     */
    private void quitLogin(HttpServletRequest request, HttpServletResponse response) {
        // 删除域中的全部数据
        //傻鸟
        // 转发到登陆页面
    }

    /**
     * 验证码验证
     */
    private void codeCheck(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 获取前端输入验证码
        String code = request.getParameter("code");
        String realCode1 = request.getParameter("realCode");
        // 获取实际验证码
        String realCode = (String) request.getAttribute("code");
        System.out.println(realCode1);
        System.out.println(realCode);
        if(code.equals(realCode1)){
            // 验证码输入正确，进入注册页面
            dispatcher("register",request,response);
        }else{
            System.out.println("错误");
            // 返回验证页面
            dispatcher("register_check",request,response);
        }
    }

    /**
     * 发送验证码
     */
    public void sendCode(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // 获取邮箱号
        String email = request.getParameter("email");
        String code = getRandom();
        // 判断邮箱是否在数据库中存在
        User user = ls.getUserInfo(email);
        if(user == null) {
            // 发送邮件
            SendMail.sendMail(email, "你的邮箱验证码为", code);
            //System.out.println("发送成功");
            request.setAttribute("code", code);
            request.setAttribute("email", email);
            // System.out.println("数据上传");
            // 重定向至注册验证页面
            dispatcher("register_check", request, response);
        }else {
            System.out.println("信息已存在，请重新输入");
            request.getRequestDispatcher("html/send_email.jsp").forward(request,response);
           //  response.sendRedirect("send_email.jsp");
        }
        // response.sendRedirect("register_check.jsp");
    }
    /**
     * 处理注册信息
     */
    private void reg(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // 创建用户对象
        User user  = new User();
        // 获取前端数据
        String nikeName = request.getParameter("nick_name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        // 保存id
        user.setId(getId());
        // 生成默认图片地址
        user.setUser_avatar("/upload/01.jpg");
        // 存入个性标签
        user.setUser_signature("无");
        user.setPassword(password);
        user.setUser_email(email);
        user.setNick_name(nikeName);
        boolean result = ls.addUserInfo(user);
        if(result){
            // 注册成功，进入登录页面
            request.getRequestDispatcher("html/login.jsp").forward(request,response);
        }else{
            // 失败，重新注册
            dispatcher("register",request, response);
        }
    }

    /**
     *  处理登录信息
     */
    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // 获取邮箱和密码
        String userEmail = request.getParameter("user_email");
        String password = request.getParameter("password");
        // 获取标签service对象
        TagService ts = new TagService();
        // 后期记得完善验证码功能
        User user = ls.getUserInfo(userEmail);
        if(user != null){
            // 进行密码验证
            if(user.getPassword().equals(password)){
                // 密码和用户名正确
                // 判断是否是第一次登录
                if(user.getSign_nums() == 1){
                    // 第一次登录，进入选择标签页面
                }else {
                    // 不是第一次登录，将标签信息和用户信息丢进域中
                    // 获取用户对应标签信息
                    List<PersonalTag> tags = ts.getTags(user.getId());
                    // 将标签信息存入域中
                    request.getSession().setAttribute("tags",tags);
                    // 将用户信息存进域中
                    request.getSession().setAttribute("user",user);
                    System.out.println("登录成功");
                    // 跳转页面进入主页
                    request.getRequestDispatcher("index?method=newsList").forward(request,response);
                   // response.sendRedirect("index.jsp");newsIndex?method=newsList
                   // response.sendRedirect("index?method=newsList");
                    return;
                }
            }
            else{
                System.out.println("密码错误");
                // 密码错误，前端显示提示信息
            }
        }
        else{
            System.out.println("用户名不存在");
            // 用户名不存在，提示信息
        }
    }
}
