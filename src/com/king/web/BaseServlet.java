package com.king.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * ClassName:BaseServlet
 * Package:com.king.web
 * Description:
 *
 * @Data:2021/9/26 23:00
 */
public class BaseServlet extends HttpServlet {
    public static final String PREFIX = "/WEB-INF/admin/";//后台页面的前缀
    public static final String SUFFIX = ".jsp";//后台页面的后缀
    // 通用转发方法
    public void dispatcher(String path, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 转发请求至指定页面
        request.getRequestDispatcher(PREFIX + path + SUFFIX).forward(request, response);
    }
    // 返回当前系统时间
    public String getNowDate(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String format = dateFormat.format(date);
        return format;
    }
    // 随机获取六位数字
    public String getRandom(){
        Random rd = new Random();
        Integer num = rd.nextInt(899999)+ 100000;
        return num.toString();
    }
    // 获取UUID
    public String getId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
