package com.king.web;

        import com.king.dao.NewsListInfoDao;
        import com.king.entity.NewsListInfo;

        import javax.servlet.ServletException;
        import javax.servlet.ServletRequest;
        import javax.servlet.ServletResponse;
        import javax.servlet.annotation.WebServlet;
        import javax.servlet.http.HttpServletRequest;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.util.List;
@WebServlet("/index")
public class NewsListInfoServlet extends BaseServlet{
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String method = "";
        method = request.getParameter("method");
        //System.out.println(method);
        switch (method) {
            case "newsList":
                setNewsListInfo(request,response);
                break;
            default:
                break;
        }
    }

   /* @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        String method = "";
        method = request.getParameter("method");
        System.out.println(method);
        switch (method) {
            case "newsList":
                setNewsListInfo(request,response);
                break;
            default:
                break;
        }
    }*/
    public void setNewsListInfo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        List<NewsListInfo> newsListInfoList = null;
        newsListInfoList = NewsListInfoDao.getNewsListInfo();
        request.setAttribute("newsListInfoList",newsListInfoList);
        request.getRequestDispatcher("index.jsp").forward(request,response);
      //  response.sendRedirect("index.jsp");
    }
}
