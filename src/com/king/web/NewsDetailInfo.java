package com.king.web;

import com.king.entity.NewsDetail;
import com.king.service.DetailInfoService;
import com.king.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * ClassName:NewsDetailInfo
 * Package:com.king.web
 * Description:
 *
 * @Data:2021/9/30 11:24
 */
@WebServlet("/newsDetail")
public class NewsDetailInfo extends BaseServlet {
            private DetailInfoService ds = new DetailInfoService();
            @Override
            protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                String method = request.getParameter("method");
                method = StringUtils.isBlank(method) ? "newsDetailInfo" : method;
                switch (method){
                    case "newsDetailInfo": outNewsInfo(request,response);
                }
            }

            /**
             * 获取字符串输出新闻代码
             */
            private void outNewsInfo(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
                // 获取新闻id
             //   System.out.println("来了老弟");
                String newsId = request.getParameter("news-id");
                NewsDetail newInfo = ds.getNewInfo(newsId);
                // 将代码输出到前端
                request.setAttribute("newInfo",newInfo);
                // 转发
                request.getRequestDispatcher("html/news_info.jsp").forward(request,response);
              //  response.sendRedirect("html/news_info.jsp");
            }
}
