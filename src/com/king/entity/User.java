package com.king.entity;

public class User {
    private String id;//唯一标识符
    private String username;//账号
    private String password;//密码
    private String user_phone;//手机号
    private String user_email;//邮箱
    private String user_sex;//用户性别
    private String user_avatar;//头像
    private String user_classes;//用户级别
    private int sign_nums;//登录次数
    private String nick_name;//昵称
    private String user_signature;//个性签名

    public User() {
    }

    public User(String id, String username, String password, String user_phone, String user_email, String user_sex, String user_avatar, String nick_name, String user_signature) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.user_phone = user_phone;
        this.user_email = user_email;
        this.user_sex = user_sex;
        this.user_avatar = user_avatar;
        this.nick_name = nick_name;
        this.user_signature = user_signature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_classes() {
        return user_classes;
    }

    public void setUser_classes(String user_classes) {
        this.user_classes = user_classes;
    }

    public int getSign_nums() {
        return sign_nums;
    }

    public void setSign_nums(int sign_nums) {
        this.sign_nums = sign_nums;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getUser_signature() {
        return user_signature;
    }

    public void setUser_signature(String user_signature) {
        this.user_signature = user_signature;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", user_phone='" + user_phone + '\'' +
                ", user_email='" + user_email + '\'' +
                ", user_sex='" + user_sex + '\'' +
                ", user_avatar='" + user_avatar + '\'' +
                ", user_classes='" + user_classes + '\'' +
                ", sign_nums=" + sign_nums +
                ", nick_name='" + nick_name + '\'' +
                ", user_signature='" + user_signature + '\'' +
                '}';
    }
}
