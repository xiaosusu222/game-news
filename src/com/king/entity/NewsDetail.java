package com.king.entity;

/**
 * ClassName:NewsDetail
 * Package:com.king.entity
 * Description:
 *
 * @Data:2021/10/9 9:54
 */
public class NewsDetail {
    private String new_title; // 新闻标题
    private String content; // 新闻内容
    private String nick_name; // 新闻作者
    private String news_add_time; // 新闻发布时间

    public NewsDetail(){}

    public NewsDetail(String new_title, String content, String nick_name, String news_add_time) {
        this.new_title = new_title;
        this.content = content;
        this.nick_name = nick_name;
        this.news_add_time = news_add_time;
    }

    public String getNew_title() {
        return new_title;
    }

    public void setNew_title(String news_title) {
        this.new_title = news_title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getNews_add_time() {
        return news_add_time;
    }

    public void setNews_add_time(String news_add_time) {
        this.news_add_time = news_add_time;
    }
}