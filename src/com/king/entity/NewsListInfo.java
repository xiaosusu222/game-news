package com.king.entity;

/**
 * 新闻列表信息实体
 */
public class NewsListInfo {
    private String id;
    private String newsTitle;
    private String titleContent;
    private String coverImage;
    private String newsUploadTime;

    public NewsListInfo() {
    }

    public NewsListInfo(String id, String newsTitle, String titleContent, String coverImage, String newsUploadTime) {
        this.id = id;
        this.newsTitle = newsTitle;
        this.titleContent = titleContent;
        this.coverImage = coverImage;
        this.newsUploadTime = newsUploadTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getTitleContent() {
        return titleContent;
    }

    public void setTitleContent(String titleContent) {
        this.titleContent = titleContent;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getNewsUploadTime() {
        return newsUploadTime;
    }

    public void setNewsUploadTime(String newsUploadTime) {
        this.newsUploadTime = newsUploadTime;
    }
}
