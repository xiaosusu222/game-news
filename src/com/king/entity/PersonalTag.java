package com.king.entity;

/**
 * ClassName:PersonalTag
 * Package:com.king.entity
 * Description:
 *
 * @Data:2021/9/27 23:33
 */
public class PersonalTag {
    private String id;
    private String user_id;
    private String type_id;
    private String type_name;

    public PersonalTag(){}

    public PersonalTag(String id, String user_id, String type_id, String type_name) {
        this.id = id;
        this.user_id = user_id;
        this.type_id = type_id;
        this.type_name = type_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }
}
