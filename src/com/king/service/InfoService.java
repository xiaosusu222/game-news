package com.king.service;

import com.king.dao.UserDao;
import com.king.entity.User;

public class InfoService {
    private UserDao ud = new UserDao();

    //将修改的user存入数据库
    public boolean updUserInfo(User user) {
        return ud.updUser(user);
    }
    //判断密码是否正确
    public boolean checkPwd(String nowpwd) {
        return (ud.getUserByPwd(nowpwd)==null)?false:true;
    }
}
