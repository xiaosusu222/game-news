package com.king.service;

import com.king.dao.PersonalTagDao;
import com.king.entity.PersonalTag;

import java.util.List;

/**
 * ClassName:TagService
 * Package:com.king.service
 * Description:
 *
 * @Data:2021/9/27 23:21
 */
public class TagService {
    PersonalTagDao tagDao = new PersonalTagDao();
    public List<PersonalTag> getTags(String id) {
        return tagDao.getTagsByUserId(id);
    }
}
