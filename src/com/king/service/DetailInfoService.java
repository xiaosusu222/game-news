package com.king.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import com.king.dao.NewsDao;
import com.king.entity.NewsDetail;
import sun.applet.Main;

import javax.print.attribute.standard.OrientationRequested;
import java.util.Iterator;
import java.util.Map;

/**
 * ClassName:DetailInfoService
 * Package:com.king.service
 * Description:
 *
 * @Data:2021/9/30 12:36
 */
public class DetailInfoService {
    NewsDao newsDao = new NewsDao();


    public NewsDetail getNewInfo(String newsId) {
        // 通过新闻id获取新闻内容
        // 需要的用户数据：
        // 新闻标题，新闻内容，作者名，发布时间
        // 左边状态栏
        // 点赞数,评论数,浏览量,转发(分享),收藏(包括显示被收藏数)
        NewsDetail newsDetail = newsDao.getNewsDetail(newsId);
       // System.out.println(newsDetail.getNew_title());
        // 处理创建时间
        newsDetail.setNews_add_time(dealCreatTime(newsDetail.getNews_add_time()));
        // 处理新闻内容
        newsDetail.setContent(dealContent(newsDetail.getContent()));
        return newsDetail;
    }
    // 处理新闻时间信息
    public String dealCreatTime(String creatTime){
        try {
            return creatTime.substring(0,10);
        }catch (Exception e){
            System.out.println("错误信息，返回原时间");
            return creatTime;
        }
    }
    // 处理新闻内容信息
    private String dealContent(String newsContent) {
      //  System.out.println(newsContent);
     //   JSONObject jsonObject = new JSONObject(16,true).parseObject(newsContent);
        // 将字符串解析为json对象JSONObject respondeBodyJson = JSONObject.parseObject(jsonStr, Feature.OrderedField);
        JSONObject jsonObject = JSON.parseObject(newsContent, Feature.OrderedField);
      //  System.out.println(jsonObject.toString());
        StringBuilder newStr = new StringBuilder();
        for(String s : jsonObject.keySet()){
           // System.out.println(s);
            if(s.contains("p")){
                newStr.append("<p style=\"text-align: justify;\">"
                        +jsonObject.getString(s)+"</p>");
            }else if(s.contains("img")){
                newStr.append("<img lay-src=\""+jsonObject.getString(s)+"\"  layer-pid layer-index=\"0\" alt=\"xiaosusu\">\n" +
                        "            <br/><br/>");
            }
            else{
                break;
            }
        }
        return newStr.toString();
    }

/*    public static void main(String[] args) {
        DetailInfoService d = new DetailInfoService();
        String s = d.dealContent("{\"p_1\":\"　　《守望先锋2》已经确认会包含PvP和PvE内容，不过仍一些有玩家好奇新作中到底更侧重哪一部分，以及PvE是否会像现在的《守望先锋》那样只是一些合作任务。近日游戏总监杰夫·卡普兰就在国外论坛上回答了这些问题。\",\"img-src_1\":\"https://img2.ali213.net/picfile/News/2020/02/04/584_2020020460317290.png\",\"p_2\":\"　　杰夫表示：“两种模式都很重要。PVE中将通过剧情任务，补全完整的世界线（尽管这是线性故事，但我不愿意将其称作战役模式，因为它是合作游戏而不是单人游戏。）我们也会有英雄任务（由成就系统支撑的、有高度重复可玩性的模式），类似于《暗黑3》中的冒险模式和《魔兽世界》中的世界任务（比喻不是很恰当，但重复可玩性跟这两者一样高）”\",\"img-src_2\":\"https://img2.ali213.net/picfile/News/2020/02/04/584_2020020460313436.jpg\",\"p_3\":\"　　“PVP模式我们也加入全新的地图，以及一个名为PUSH的新游戏模式。并且两种游玩模式，我们都将加入新英雄。”\",\"img-src_3\":\"https://img2.ali213.net/picfile/News/2020/02/04/584_2020020460314755.jpg\",\"p_4\":\"　　从之前的演示来看，《守望先锋2》的PvE内容肯定比1代中充足很多，有独立的角色升级系统和装备道具系统。不过目前游戏发售日尚未公布，不知道《守望先锋2》到底会不会好玩？\",\"p_5\":\"更多内容：守望先锋2专题守望先锋2论坛\"}");
       // System.out.println(s);
        NewsDetail newInfo = d.getNewInfo("00023f211a4a4ef09ab5dfa4f2f6ee06");
        System.out.println(newInfo.getNew_title());
        System.out.println(newInfo.getNick_name());
        System.out.println(newInfo.getContent());
        System.out.println(newInfo.getNews_add_time());
    }*/
}
