package com.king.service;

import com.king.dao.UserDao;
import com.king.entity.User;

/**
 * ClassName:LoginSerivce
 * Package:com.king.service
 * Description:
 *
 * @Data:2021/9/27 22:50
 */
public class LoginService {
    private UserDao us = new UserDao();

    public User getUserInfo(String userEmail) {
        return us.getUserByUserEmail(userEmail);
    }

    public boolean addUserInfo(User user) {
        return us.addUser(user);
    }
}
