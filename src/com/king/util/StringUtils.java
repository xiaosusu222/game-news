package com.king.util;

import java.util.Date;
import java.util.UUID;

public class StringUtils {

    /**
     * 判断一个字符串不为空
     * "null"字符串也会被认为是空
     */
    public static boolean isNotBlank(String s){
        return s != null && !s.equals("") && !s.equals("null");
    }

    /**
     * 判断一个字符串为空
     * "null"字符串也会被认为是空
     */
    public static boolean isBlank(String s){
        return s == null || s.equals("") || "null".equals(s);
    }

    /**
     * 随机生成一个文件名称（当前时间的毫秒数+一段16进制的数字）
     */
    public static String randName(){
        return new Date().getTime() + randUUIDSubstr();
    }

    /**
     * 只取uuid的-最后一段字符
     */
    public static String randUUIDSubstr(){
        //获取一段随机的uuid
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.substring(uuid.lastIndexOf("-"));
        return uuid;
    }


    /**
     * 随机生成一个UUID，去掉了-
     */
    public static String randUUIDStr(){
        //获取一段随机的uuid
        return UUID.randomUUID().toString().replaceAll("\\-", "");
    }

    /**
     * 获取一个文件的后缀
     */
    public static String getSuffix(String fileName){
        return fileName.substring(fileName.lastIndexOf("."));
    }


    /**
     * 如果参数字符串为null或者"null"，就直接返回空""
     */
    public static String parseNull(String s) {
        return (s == null || "null".equals(s)) ? "" : s;
    }




}
