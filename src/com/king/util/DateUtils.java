package com.king.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private static SimpleDateFormat sdf = new SimpleDateFormat();

    /**
     * 传入一个模板，获取当前的格式化时间
     * @param pattern 模板，如果不传，默认是yyyy-MM-dd HH:mm:ss
     */
    public static String currentTime(String pattern){
        //如果传入的模板不为空，则加载新模板
        sdf.applyPattern(StringUtils.isBlank(pattern) ? "yyyy-MM-dd HH:mm:ss" : pattern);
        return sdf.format(new Date());
    }

    /**
     * 获取当前的格式化时间，格式固定为yyyy-MM-dd HH:mm:ss
     */
    public static String currentTime(){
        sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

}
