package com.king.util;

import java.util.Calendar;
import java.util.Random;
import java.util.UUID;

public class RandomUtils {

    private static Random r = new Random();

    /**
     * 随机生成一个区间内的数字
     * @param left 左区间
     * @param right 右区间
     */
    public static int randSectionNum(int left, int right){
        return r.nextInt(right - left) + left;
    }

    /**
     * 随机生成订单编号，规则eb+年月日时分秒毫秒+随机四个数
     */
    public static String createOrderId() {
        Calendar c = Calendar.getInstance();
        //不要eb，丑死
        return new StringBuilder().append(c.get(Calendar.YEAR))
                .append(convertTime((c.get(Calendar.MONTH) + 1)))
                .append(convertTime(c.get(Calendar.DAY_OF_MONTH)))
                .append(convertTime(c.get(Calendar.HOUR_OF_DAY)))
                .append(convertTime(c.get(Calendar.MINUTE)))
                .append(convertTime(c.get(Calendar.SECOND)))
                .append(convertMillis(c.get(Calendar.MILLISECOND)))
                .append(r.nextInt(9000) + 1000).toString();
    }

    /**
     * 月、日、时、分、秒，如果没有超过两位数，就加个0
     */
    public static String convertTime(int time){
        return time > 9 ? time+"" : "0" + time;
    }

    /**
     * 毫秒数三位数，如果只有两位数就，就加一个0，一位数就加两个0
     */
    public static String convertMillis(int millis){
        return (millis < 10) ? "00" + millis : ((millis < 100) ? "0" + millis : millis+"");
    }

    /**
     * 随机生成一段uuid的盐值
     */
    public static String randSalt(){
        return UUID.randomUUID().toString().replaceAll("\\-", "").toUpperCase();
    }

    /**
     * 随机生成加密次数
     * 100-999之间
     */
    public static int randMCount(){
        //0-899 + 100 --> 100-999
        return r.nextInt(900) + 100;
    }

}
