package com.king.util;

import java.text.DecimalFormat;

public class NumberUtils {

    private static DecimalFormat df = new DecimalFormat();
    /**
     * 将小数进行格式化，默认保留两位小数
     * @param num 需要格式化的数据
     * @param pattern 数字模板
     */
    public static double toFix(double num, String pattern){
        df.applyPattern(StringUtils.isBlank(pattern) ? "#.##" : pattern);
        return Double.parseDouble(df.format(num));
    }

    /**
     * 将小数进行格式化，只能保留两位小数
     * @param num 需要格式化的数据
     */
    public static double toFix(double num){
        df.applyPattern("#.##");
        return Double.parseDouble(df.format(num));
    }


    /**
     * 如果int数字转化异常，就返回0
     * @param strNum 要转化的字符串
     * @return 转化之后的字符串
     */
    public static int parseInt(String strNum) {
        try {
            return Integer.parseInt(strNum);
        } catch (NumberFormatException e) {
            return 0;//数字转化异常
        }
    }

    /**
     * 如果double数字转化异常，就返回0
     */
    public static double parseDouble(String strNum) {
        try {
            return Double.parseDouble(strNum);
        } catch (NumberFormatException e) {
            return 0;//数字转化异常
        }
    }

}
