package com.king.util;


import javax.servlet.http.Part;
import java.io.*;
import java.net.URLDecoder;
import java.util.List;

public class FileUtils {

    public static final String SAVE_PIC = "D:\\工作空间\\项目保存文件\\rz1117-1203";

    /**
     * 根据文件包part获取对应的存储路径
     * @param part 前端上传的文件对象
     * @param dirName 单独的文件夹对象
     */
    public static String getSavePath(Part part, String dirName) {
        //判断文件包是否存在，是否点击了上传文件
        //如果没有上传文件
        if(!isPart(part)){
            return null;
        }
        //创建file对象，如果路径不存在，就创建
        File file = new File(SAVE_PIC + "\\" + dirName);
        if (!file.exists()) {
            file.mkdirs();
        }
        //自己生成一个随机的名称
        return SAVE_PIC + "\\" + dirName + "\\"
                + StringUtils.randName() + StringUtils.getSuffix(part.getSubmittedFileName());
    }

    /**
     * 判断是否上传了文件
     */
    public static boolean isPart(Part part){
        return part != null && part.getContentType() != null
                && StringUtils.isNotBlank(part.getSubmittedFileName());
    }

    /**
     * 删除单个文件信息
     * @param pic
     */
    public static void deleteFile(String pic) {
        if (StringUtils.isNotBlank(pic)) {
            File file = new File(pic);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /**
     * 删除多个文件信息-根据photo的实体类对象
     * @param photos
     */
    /*public static void deleteFile(List<Photo> photos) {
        for (Photo photo : photos) {
            deleteFile(photo.getPhotoPath());
        }
    }*/

    /**
     * 删除多个文件信息-直接根据所有地址的字符串
     */
    public static void deleteFilePath(List<String> paths) {
        for (String path : paths) {
            deleteFile(path);
        }
    }


    /**
     * 根据Part文件对象和path存储路径，保存图片
     * @param part 上传的文件对象
     * @param path 存储路径
     */
    public static void savePicDisk(Part part, String path) {
        //判断是不是可以保存的文件，并且part是否为空
        if (isPart(part) && StringUtils.isNotBlank(path)){
            try {
                part.write(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据Part文件对象和path存储路径，保存图片
     * @param parts 多个上传的文件对象
     * @param jsonArray 多个存储路径
     */
    /*public static void savePicDisk(Collection<Part> parts, JSONArray jsonArray) {
        //判断是不是可以保存的文件，并且part是否为空
        List<Photo> photos = JSONObject.parseArray(jsonArray.toString(), Photo.class);
        int i = 0;
        //循环每一个文件和对应的存储路径
        for (Part part : parts) {
            savePicDisk(part, photos.get(i++).getPhotoPath());
        }
    }*/

    /**
     * 保存文件
     */
    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        //保存
        byte[] b = new byte[1024];
        //高效输入流
        BufferedInputStream bin = new BufferedInputStream(in);
        //高效输出流
        BufferedOutputStream bout = new BufferedOutputStream(out);
        //循环写入
        int len;
        while ((len = bin.read(b)) != -1) {
            bout.write(b, 0, len);
        }
        bout.flush();
        bout.close();
        bin.close();
    }

    /**
     * 获取当前项目所在的跟目录
     */
    public static String getRootPath(){
        //获取FileUtils.class文件所在的跟目录，com的上一级
        //使用Java代码把中文编码
        //将路径中的url编码进行解码[编码中文和某些特殊符号(+)]
        try {
            return URLDecoder.decode(
                    FileUtils.class.getResource("/").getPath(),
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
