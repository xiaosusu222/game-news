package com.king.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {
	
	public static void sendMail(String email, String title, String content) {
		try {
			//发送邮件步骤：
			//1、导入mail.jar包
			//3、进入要发邮件的邮箱，打开三方协议POP3/SMPT/IMAP
			//4、设置授权码,该密码为代码中使用的密码，与登录密码不一样
			//0.1 设置基本参数
			Properties props = new Properties();
			//  设置主机
			props.setProperty("mail.host", "smtp.163.com");
			//  确定使用权限验证
			props.setProperty("mail.smtp.auth", "true");
			//0.2 确定账号与密码 
			Authenticator authenticator = new Authenticator() {
				@Override
				protected PasswordAuthentication getPasswordAuthentication() {
					//不是密码NDPXPHXUSTBTAKKV
					return new PasswordAuthentication("swj1754972569swj@163.com", "NDPXPHXUSTBTAKKV");
				}
			};
			//1获得连接
			Session session = Session.getInstance(props, authenticator);
			//2 编写消息
			Message message = new MimeMessage(session);
			// 2.1 发件人panpan2016ok
			message.setFrom(new InternetAddress("swj1754972569swj@163.com","新闻资讯提示您"));

			// 2.2 收件人 , to:收件人   cc:抄送   bcc:暗送
			 message.setRecipient(RecipientType.TO, new InternetAddress(email));
			
			//设置多个收件人地址
			//message.addRecipient(RecipientType.TO,new InternetAddress("设置收件人的邮箱"));
			 
			// 2.3 主题
			message.setSubject(title);
			// 2.4 正文
			message.setContent(content, "text/html;charset=UTF-8");
			//3发送消息
			Transport.send(message);
			System.out.println("发送成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		// 轰炸你们
		int i = 10;
		// 1417519962 zhang
		// 2468437354 dd
		// 398879431 chen
		// 2268245819 luo
		System.out.println("准备发送");
		sendMail("1158136147@qq.com", "邱邱", "swj");
		while(i>0) {
			String content = "邱邱，学习了";
			System.out.println("正在发送");
			sendMail("1158136147@qq.com", "帅哥来了", content);
			System.out.println("发送成功");
			i--;
		}
		int j = 10;
		while(j>0) {
			String content = "学校能回来了嘛";
			System.out.println("正在发送");
			sendMail("1158136147@qq.com", "帅哥又来了", content);
			System.out.println("发送成功");
			j--;
		}
	}
}
