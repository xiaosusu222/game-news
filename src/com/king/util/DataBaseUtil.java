package com.king.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DataBaseUtil {
    static{//第一次操作DBUtil时自动执行，并且只执行一次
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static final String URL = "jdbc:mysql://rm-bp13o9j8njzno2m674o.mysql.rds.aliyuncs.com:3306/gc1223_3?useSSL=false&useUnicode=true&characterEncoding=UTF-8";
    public static final String USER = "gc1223_3";
    public static final String PASSWORD = "gc1223_3";
    public static Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL,USER,PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
    //关资源
    public static void close(Connection conn,PreparedStatement ps,ResultSet rs) {
        try {
            if(rs!=null)
                rs.close();
            if(ps!=null)
                ps.close();
            if(conn!=null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
