<%--
  Created by IntelliJ IDEA.
  User: 11712
  Date: 2021/10/2
  Time: 20:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>文章发布</title>
    <meta name="keywords" content="前端模板">
    <meta name="description" content="前端模板">
    <%@include file="../../html/_include/resource.jsp"%>
</head>
<body>
<%@include file="../../html/_include/header.jsp"%>

<div class="layui-container container">
    <h2 class="page-title">发表文章</h2>
    <div class="layui-form layui-form-pane">
        <form action="#" method="post">
            <div class="layui-form-item">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item layui-form-text">
                <div class="layui-input-block">
                    <textarea id="content" name="content" lay-verify="content" placeholder="请输入内容" class="layui-textarea layui-hide"></textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">文章分类</label>
                    <div class="layui-input-block">
                        <select name="type" lay-verify="type">
                            <option value=""></option>
                            <option value="0">写作</option>
                            <option value="1">阅读</option>
                            <option value="2">游戏</option>
                            <option value="3">音乐</option>
                            <option value="4">旅行</option>
                        </select>
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label">验证码</label>
                    <div class="layui-input-block">
                        <input type="text" name="code" lay-verify="code" autocomplete="off" placeholder="验证码" class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <button class="layui-btn" lay-filter="*" lay-submit>立即发布</button>
            </div>
        </form>
    </div>
</div>
<%@include file="../../html/_include/footer.jsp"%>
</body>
</html>
