<%--
  Created by IntelliJ IDEA.
  User: 17549
  Date: 2021/9/27
  Time: 22:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Classy Register Form Responsive Widget Template :: W3layouts</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Classy Register Form Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
    function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Meta tag Keywords -->
    <!-- css files -->
    <link href="${basePath}static/css/style2.css" rel="stylesheet" type="text/css" media="all">
    <!-- //css files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Cuprum:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
    <!--//online-fonts -->
</head>
<body>
<div class="header">
    <h1>欢迎来到注册页面</h1>
</div>
<div class="w3-main">
    <!-- Main -->
    <div class="about-bottom main-agile book-form">
        <div class="alert-close"> </div>
        <h2 class="tittle">注册信息</h2>
        <form action="${basePath}login?method=reg" method="post">
            <div class="form-date-w3-agileits">
                <label> 姓名 </label>
                <input type="text" name="nick_name" placeholder="Your Name" required="">
                <label> 邮箱 </label>
                <input type="email" name="email" placeholder="Your Email" required="">
                <label> 密码 </label>
                <input type="password" name="password" placeholder="Your Password" required="">
                <label> 确认密码 </label>
                <input type="password" name="password" placeholder="Confirm Password" required="">
            </div>
            <div class="make wow shake">
                <input type="submit" value="Register">
            </div>
        </form>
    </div>
    <!-- //Main -->
</div>
<!-- footer -->
<div class="footer-w3l">
    <p>&copy; 2017 Classy Register Form. All rights reserved | Design by <a href="http://w3layouts.com">W3layouts</a></p>
</div>
<!-- //footer -->
<!-- js-scripts-->
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script>$(document).ready(function(c) {
    $('.alert-close').on('click', function(c){
        $('.main-agile').fadeOut('slow', function(c){
            $('.main-agile').remove();
        });
    });
});
</script>
<!-- //js-scripts-->
</body>
</html>