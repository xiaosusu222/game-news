<%--
  Created by IntelliJ IDEA.
  User: 17549
  Date: 2021/9/26
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>404</title>
    <meta name="keywords" content="前端模板">
    <meta name="description" content="前端模板">
    <script src="${basePath}static/js/jquery/jquery-2.1.4.min.js" type="text/javascript"></script>
    <script src="${basePath}static/layui/layui.js" type="text/javascript"></script>
    <script src="${basePath}static/js/index/index.js" type="text/javascript"></script>
    <script src="${basePath}static/js/index/freezeheader.js" type="text/javascript"></script>
    <script src="${basePath}static/layui/lay/modules/layer.js" type="text/javascript"></script>
    <script src="${basePath}static/js/index/sliders.js" type="text/javascript"></script>
    <script src="${basePath}static/js/index/html5.js" type="text/javascript"></script>
    <link rel="stylesheet" href="${basePath}static/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${basePath}static/layui/css/modules/layer/default/layer.css"/>
    <link rel="stylesheet" href="${basePath}static/css/global.css"/>
</head>
<body>

<div class="layui-container container">
    <div class="fly-none">
        <h2><i class="iconfont icon-404"></i></h2>
        <p>你瞧！页面或者数据被<a href="#"> 纸飞机 </a>运到火星了</p>
    </div>
</div>
<div class="footer">
    <hr class="layui-bg-red">
    <p><a href="http://itdaima.com/">Loveing’Game Hot News</a> 2017 &copy; <a href="#">itdaima.com</a></p>
    <p>Loveing’Game Hot News</p>
</div></body>
</html>
