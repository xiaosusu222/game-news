
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--<%@include file="base.jsp"%>--%>
<html>
  <head>
    <%@ include file="html/_include/resource.jsp"%>
    <title>首页</title>
  </head>
  <body>
  <%@ include file="html/_include/header.jsp"%>
  <div class="layui-container container">
    <div class="layui-row layui-col-space20">
      <div class="layui-col-md8">
        <div class="carousel">
          <div class="layui-carousel" id="images-carousel" lay-anim="fade" lay-indicator="inside" lay-arrow="always" style="width: 100%; height: 280px;">
            <div carousel-item="">
              <div class="">
                <a href="article_details.html"><img width="100%" height="280px;" src="${basePath}static/images/1.jpeg"></a>
              </div>
              <div class="">
                <img width="100%" height="280px;" src="${basePath}static/images/2.jpeg">
              </div>
              <div class="">
                <img width="100%" height="280px;" src="${basePath}static/images/3.jpg">
              </div>
              <div class="layui-this">
                <img width="100%" height="280px;" src="${basePath}static/images/4.jpg">
              </div>
              <div class="">
                <img width="100%" height="280px;" src="${basePath}static/images/5.jpg">
              </div>
            </div>
            <div class="layui-carousel-ind"><ul><li class=""></li><li class=""></li><li class=""></li><li class="layui-this"></li><li class=""></li></ul></div><button class="layui-icon layui-carousel-arrow" lay-type="sub"></button><button class="layui-icon layui-carousel-arrow" lay-type="add"></button></div>
        </div>
        <div class="article-main">
          <h2>
            文章推荐
          </h2>
          <c:forEach items="${newsListInfoList}" var="newsListInfo">
            <!-- 新闻列表(模板显示6个)-->
            <div class="article-list">
              <!-- 封面图片 -->
              <figure><img src="${newsListInfo.coverImage}"></figure>
              <ul>
                <h3>
                  <!-- 文章标题以及文章链接地址 -->
                  <a href="${basePath}/newsDetail?news-id=${newsListInfo.id}">${newsListInfo.newsTitle}</a>
                </h3>
                <!-- 文章缩略内容 -->
                <p>${newsListInfo.titleContent}</p>
                <p class="autor">
                  <!-- 上传时间 -->
                  <span class="dtime f_l">${newsListInfo.newsUploadTime}</span>
                  <!-- 浏览数 -->
                  <span class="viewnum f_r">浏览（<a href="${basePath}/newsDetail?news-id=${newsListInfo.id}">459</a>）</span>
                  <!-- 点赞数 -->
                  <span class="lm f_l">点赞（<a href="${basePath}/newsDetail?news-id=${newsListInfo.id}">1024</a>）</span>
                  <!-- 评论数 -->
                  <span class="pingl f_r">评论（<a href="${basePath}/newsDetail?news-id=${newsListInfo.id}">30</a>）</span></p>
              </ul>
            </div>
          </c:forEach>
        </div>
        <!--分页-->
        <div id="page"><div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-1"><a href="javascript:;" class="layui-laypage-prev layui-disabled" data-page="0">上一页</a><span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>1</em></span><a href="javascript:;" data-page="2">2</a><a href="javascript:;" data-page="3">3</a><a href="javascript:;" data-page="4">4</a><a href="javascript:;" data-page="5">5</a><span class="layui-laypage-spr">…</span><a href="javascript:;" class="layui-laypage-last" title="尾页" data-page="10">10</a><a href="javascript:;" class="layui-laypage-next" data-page="2">下一页</a></div></div>
      </div>
      <%@ include file="html/_include/right.jsp"%>
    </div>
  </div>
  <%@ include file="html/_include/footer.jsp"%>
  </body>
</html>
