<%--
  Created by IntelliJ IDEA.
  User: 17549
  Date: 2021/9/26
  Time: 23:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/base.jsp"%>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
-->
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Trendz Login Form Responsive Widget Template :: W3layouts</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
    <meta name="keywords"
          content="Trendz Login Form Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //Meta tag Keywords -->
    <!--/Style-CSS -->
    <link rel="stylesheet" href="${basePath}static/css/style.css" type="text/css" media="all" />
    <!--//Style-CSS -->
</head>

<body>
<!-- /login-section -->

<section class="w3l-forms-23">
    <div class="forms23-block-hny">
        <div class="wrapper">
            <h1>Trendz Login Form</h1>
            <!-- if logo is image enable this
                <a class="logo" href="index.html">
                  <img src="image-path" alt="Your logo" title="Your logo" style="height:35px;" />
                </a>
            -->
            <div class="d-grid forms23-grids">
                <div class="form23">
                    <div class="main-bg">
                        <h6 class="sec-one">John Doe</h6>
                        <div class="speci-login first-look">
                            <img src="${basePath}static/images/user.png" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="bottom-content">
                        <form action="${basePath}login?method=login" method="post">
                            <input type="email" name="user_email" class="input-form" placeholder="Your Email"
                                   required="required" value="1754972569@qq.com"/>
                            <input type="password" name="password" class="input-form"
                                   placeholder="Your Password" required="required" value="888888"/>
                            <button type="submit" class="loginhny-btn btn">Login</button>
                        </form>
                        <p>Not a member yet? <a href="send_email.jsp">Join Now!</a></p>
                    </div>
                </div>
            </div>
            <div class="w3l-copy-right text-center">
                <p>Loveing’Game Hot News
                    <a href="https://baidu.com/" target="_blank">baidu.com</a></p>
            </div>
        </div>
    </div>
</section>
<!-- //login-section -->
</body>
</html>
