
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-col-md4">
    <div class="article-fenlei">
        <h3>文章分类</h3>
        <button class="layui-btn layui-btn-warm">个人博客</button>
        <button class="layui-btn layui-btn-primary">HTML</button>
        <button class="layui-btn layui-btn-danger">JAVA</button>
        <br/>
        <br/>
        <button class="layui-btn layui-btn-primary">javascript</button>
        <button class="layui-btn layui-btn-normal">SQL</button>
        <button class="layui-btn layui-btn-primary">其他</button>
    </div>

    <div class="ad"> <img src="${basePath}static/images/ad.jpg"> </div>
    <div class="ms-top">
        <ul class="hd" id="tab">
            <li class="cur"><a>点击排行</a></li>
            <li><a>最新文章</a></li>
            <li><a>站长推荐</a></li>
            <li><a>最新评论</a></li>
        </ul>
    </div>
    <div class="ms-main" id="ms-main">
        <div style="display: block;" class="bd bd-news" >
            <ul>
                <li><a href="#" target="_blank">住在手机里的朋友</a></li>
                <li><a href="#" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                <li><a href="#" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                <li><a href="#" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                <li><a href="#" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
                <li><a href="#" target="_blank">你面对的是生活而不是手机</a></li>
            </ul>
        </div>
        <div  class="bd bd-news">
            <ul>
                <li><a href="#" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                <li><a href="#" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                <li><a href="#" target="_blank">住在手机里的朋友</a></li>
                <li><a href="#" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                <li><a href="#" target="_blank">你面对的是生活而不是手机</a></li>
                <li><a href="#" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
            </ul>
        </div>
        <div class="bd bd-news">
            <ul>

                <li><a href="#" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                <li><a href="#" target="_blank">你面对的是生活而不是手机</a></li>
                <li><a href="#" target="_blank">住在手机里的朋友</a></li>
                <li><a href="#" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
                <li><a href="#" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                <li><a href="#" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
            </ul>
        </div>
        <div  class="bd bd-news">
            <ul>
                <li><a href="#" target="_blank">原来以为，一个人的勇敢是，删掉他的手机号码...</a></li>
                <li><a href="#" target="_blank">手机的16个惊人小秘密，据说99.999%的人都不知</a></li>
                <li><a href="#" target="_blank">住在手机里的朋友</a></li>
                <li><a href="#" target="_blank">教你怎样用欠费手机拨打电话</a></li>
                <li><a href="#" target="_blank">你面对的是生活而不是手机</a></li>
                <li><a href="#" target="_blank">豪雅手机正式发布! 在法国全手工打造的奢侈品</a></li>
            </ul>
        </div>
    </div>

    <div class="tuwen">
        <h3>图文推荐</h3>
        <ul>
            <li><a href="#"><img src="${basePath}static/images/01.jpg"><b>住在手机里的朋友</b></a>
                <p><span class="tulanmu"><a href="#">手机配件</a></span><span class="tutime">2015-02-15</span></p>
            </li>
            <li><a href="#"><img src="${basePath}static/images/02.jpg"><b>教你怎样用欠费手机拨打电话</b></a>
                <p><span class="tulanmu"><a href="#">手机配件</a></span><span class="tutime">2015-02-15</span></p>
            </li>
            <li><a href="#" title="手机的16个惊人小秘密，据说99.999%的人都不知"><img src="${basePath}static/images/03.jpg"><b>手机的16个惊人小秘密，据说...</b></a>
                <p><span class="tulanmu"><a href="#">手机配件</a></span><span class="tutime">2015-02-15</span></p>
            </li>
            <li><a href="#"><img src="${basePath}static/images/06.jpg"><b>住在手机里的朋友</b></a>
                <p><span class="tulanmu"><a href="#">手机配件</a></span><span class="tutime">2015-02-15</span></p>
            </li>
            <li><a href="#"><img src="${basePath}static/images/04.jpg"><b>教你怎样用欠费手机拨打电话</b></a>
                <p><span class="tulanmu"><a href="#">手机配件</a></span><span class="tutime">2015-02-15</span></p>
            </li>
        </ul>
    </div>
    <div class="ad"> <img src="${basePath}static/images/03.jpg"> </div>
</div>
