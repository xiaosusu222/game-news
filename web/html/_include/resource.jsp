<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../../base.jsp"%>
<script src="${basePath}static/js/jquery/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="${basePath}static/layui/layui.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/index.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/freezeheader.js" type="text/javascript"></script>
<script src="${basePath}static/layui/lay/modules/layer.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/sliders.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/html5.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/article_pub.js" type="text/javascript"></script>
<script src="${basePath}static/js/index/set.js" type="text/javascript"></script>

<link rel="stylesheet" href="${basePath}static/layui/css/layui.css" media="all"/>
<link rel="stylesheet" href="${basePath}static/layui/css/modules/layer/default/layer.css"/>
<link rel="stylesheet" href="${basePath}static/css/global.css"/>
