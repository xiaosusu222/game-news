<%--
  Created by IntelliJ IDEA.
  User: 17549
  Date: 2021/9/26
  Time: 13:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-header header">
    <div class="main">
        <ul class="layui-nav layui-nav-left" lay-filter="filter">
            <a class="logo" href="index.html" title="Fly">Fly</a>
            <li class="layui-nav-item nav-left">
                <a href="index?method=newsList">首页</a>
            </li>
            <li class="layui-nav-item layui-this">
                <a href="index?method=newsList">文章</a>
            </li>
            <li class="layui-nav-item">
                <a href="time_line.html">时间轴</a>
            </li>
            <li class="layui-nav-item">
                <a href="time_line.html">相册</a>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right layui-nav-right" lay-filter="filter">

            <li class="layui-nav-item">
                <a href="home.html">我的主页<span class="layui-badge-dot"></span></a>
            </li>
            <li class="layui-nav-item">
                <a href="javascript:;"><img src="${basePath}${user.user_avatar }" class="layui-nav-img">我</a>
                <dl class="layui-nav-child">
                    <dd><a href="info?method=settingsPage">基本设置</a></dd>
                    <dd><a href="info?method=pubPage">发布文章</a></dd>
                    <dd><a href="info?method=exit">退出登录</a></dd>
                </dl>
            </li>
        </ul>
    </div>
</div>